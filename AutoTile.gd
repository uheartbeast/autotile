tool

# AutoTile.gd
extends TileMap

# Direction enum (for autotile math)
enum {
	NORTH = 1,
	WEST = 2,
	EAST = 4,
	SOUTH = 8
}

func _ready():
	update_tiles(get_used_cells())

	# Only run the process if we are in tool mode
	if get_tree().is_editor_hint():
		set_process(true)

func _process(delta):
	# Get the used cells and then update them
	# Using the process function isn't the best way to do this but I plan on optimizing this
	# as soon as I can. I just threw this part together to get it working. @Gunter did help me at least
	# optimize it a little with the get_used_cells method. Eventually I'd like to only update the tiles
	# surrounding the cell most recently change and only when a mouse buttong is being pressed.
	update_tiles(get_used_cells())

func update_tiles(cell_list):
	for cell in cell_list:
		# Check all directions to find out if there is a cell there
		var north_cell = get_cell(cell.x, cell.y-1) != -1 # Returns true or false if there is a tile
		var west_cell = get_cell(cell.x-1, cell.y) != -1 # Returns true or false if there is a tile
		var east_cell = get_cell(cell.x+1, cell.y) != -1 # Returns true or false if there is a tile
		var south_cell = get_cell(cell.x, cell.y+1) != -1 # Returns true or false if there is a tile
		
		# Get the cell index using bitmasking.
		# --------------------------------------------------------------------------------------------------------------------------------
		# METHOD USED: https://gamedevelopment.tutsplus.com/tutorials/how-to-use-tile-bitmasking-to-auto-tile-your-level-layouts--cms-25673 
		# --------------------------------------------------------------------------------------------------------------------------------
		var cell_index = NORTH*north_cell + WEST*west_cell + EAST*east_cell + SOUTH*south_cell;
		
		# Update the cell index
		set_cell(cell.x, cell.y, cell_index)